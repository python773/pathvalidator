from dataclasses import dataclass
from enum import Enum, StrEnum, auto
from pathlib import Path


class ValidationError(Exception):
    def __init__(self, message) -> None:
        self.message = message
        super().__init__(self.message)


class PathType(StrEnum):
    FILE = auto()
    DIR = auto()


@dataclass
class PathValidator:
    """Validation of input_path with input_path_type
    - converting into pathlib object if necessery
    - check if file/dir exists
    - validate type of path with class input
    """

    in_path: Path | str
    in_path_type: PathType = PathType.FILE

    def __post_init__(self):
        # * Validation
        self.status = False

        if not isinstance(self.in_path, Path):
            self.path: Path = Path(self.in_path)
        else:
            self.path = self.in_path

        # Assign and validate path type
        self.ptype = self.get_ptype()

        # Match type with input path_type
        if not self.in_path_type == self.ptype:
            raise ValidationError(
                f"For input_path: {self.path} - {self.ptype}\n"
                f"delivered not matching input_path_type: {self.in_path_type}"
            )

        self.status = True
        # *

    def get_ptype(self):
        """Return type of in_path"""

        if not self.path.exists():
            raise FileExistsError(f"Not exist input_path")
        elif self.path.is_file():
            return "file"
        elif self.path.is_dir():
            return "dir"

    @property  # return validation status
    def valid(self) -> bool:
        return self.status

    @property  # returner pathlibpath
    def ppath(self) -> Path:
        return self.path
