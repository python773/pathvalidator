from pathlib import Path

import pytest

from path_validator import PathType, PathValidator, ValidationError


def test_input_str_dir() -> None:
    in_path = r"C:\Program Files\Python38"
    in_path_type = PathType.DIR

    assert isinstance(PathValidator(in_path, in_path_type).ppath, Path)


def test_input_Path_dir() -> None:
    in_path = Path(r"C:\Program Files\Python38")
    in_path_type = PathType.DIR

    assert isinstance(PathValidator(in_path, in_path_type).ppath, Path)


def test_input_str_file() -> None:
    in_path = r"C:\Program Files\Python38\python.exe"
    in_path_type = PathType.FILE

    assert isinstance(PathValidator(in_path, in_path_type).ppath, Path)


def test_input_Path_file() -> None:
    in_path = Path(r"C:\Program Files\Python38\python.exe")
    in_path_type = PathType.FILE

    assert isinstance(PathValidator(in_path, in_path_type).ppath, Path)


def test_input_valid_dir() -> None:
    in_path = r"C:\Program Files\Python38"
    in_path_type = PathType.DIR

    assert PathValidator(in_path, in_path_type).status == True


def test_not_exist_dir() -> None:
    with pytest.raises(FileExistsError):
        in_path = r"C:\Program Files\Python381"
        in_path_type = PathType.DIR
        PathValidator(in_path, in_path_type)


def test_input_valid_file() -> None:
    in_path = r"C:\Program Files\Python38\python.exe"
    in_path_type = PathType.FILE

    assert PathValidator(in_path, in_path_type).status == True


def test_not_exist_file() -> None:
    with pytest.raises(FileExistsError):
        in_path = r"C:\Program Files\Python38\python1.exe"
        in_path_type = PathType.FILE
        PathValidator(in_path, in_path_type)


def test_not_proper_in_path_type1() -> None:
    with pytest.raises(ValidationError):
        in_path = r"C:\Program Files\Python38\python.exe"
        in_path_type = PathType.DIR
        PathValidator(in_path, in_path_type)


def test_not_proper_in_path_type2() -> None:
    with pytest.raises(ValidationError):
        in_path = r"C:\Program Files\Python38"
        in_path_type = PathType.FILE
        PathValidator(in_path, in_path_type)
